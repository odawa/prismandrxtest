﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAndRxTest
{
    public class AppContext
    {
        public Preference Preference { get; private set; }

        public AppContext()
        {
            Preference = new Preference();
            Preference.Background = Values.BackgroundColors.Blue;
        }
    }
}
