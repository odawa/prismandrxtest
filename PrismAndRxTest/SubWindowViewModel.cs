﻿using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Reactive.Linq;

namespace PrismAndRxTest
{
    public class SubWindowViewModel
    {
        public ReactiveProperty<SolidColorBrush> Background { get; private set; }

        private Preference _preference;
        
        public SubWindowViewModel(Preference preference)
        {
            if (preference == null) throw new ArgumentNullException("preference is null.");

            _preference = preference;

            Background = preference.ObserveProperty(p => p.Background)  // PreferenceモデルのBackgroundプロパティを
                .Select(bg => ToBrush(bg))                              // ToBrushで変換するように指定して
                .ToReactiveProperty<SolidColorBrush>();                 // ReactivePropertyに変換してバインド
        }

        // Converterを用意してもいいかもしれないが、Selectの動作の確認のためにメソッドで実装
        private SolidColorBrush ToBrush(Values.BackgroundColors color)
        {
            SolidColorBrush brush = Brushes.White;
            switch (color)
            {
                case Values.BackgroundColors.White:
                    brush = Brushes.White;
                    break;
                case Values.BackgroundColors.Red:
                    brush = Brushes.Red;
                    break;
                case Values.BackgroundColors.Green:
                    brush = Brushes.Green;
                    break;
                case Values.BackgroundColors.Blue:
                    brush = Brushes.Blue;
                    break;
                case Values.BackgroundColors.Glay:
                    brush = Brushes.LightGray;
                    break;
                default:
                    break;
            }
            return brush;
        }
    }
}
