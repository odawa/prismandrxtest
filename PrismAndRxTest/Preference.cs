﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAndRxTest
{
    public class Preference : BindableBase
    {
        private bool _featureAIsEnabled = false;
        public bool FeatureAIsEnabled
        {
            get { return _featureAIsEnabled; }
            set
            {
                Console.WriteLine("FeatureAIsEnabled:{0}", value);
                SetProperty<bool>(ref _featureAIsEnabled, value);
            }
        }

        private Values.BackgroundColors _Background = Values.BackgroundColors.White;
        public Values.BackgroundColors Background
        {
            get { return _Background; }
            set { SetProperty<Values.BackgroundColors>(ref _Background ,value); }
        }
    }
}
