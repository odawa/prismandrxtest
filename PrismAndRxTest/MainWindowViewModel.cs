﻿using Reactive.Bindings;
using Reactive.Bindings.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;

namespace PrismAndRxTest
{
    class MainWindowViewModel
    {
        public ReactiveProperty<bool> FeatureAIsEnabled { get; private set; }
        public ReactiveProperty<Values.BackgroundColors> SelectedBackground { get; private set; }

        private ObservableCollection<Values.BackgroundColors> _backgroundList = new ObservableCollection<Values.BackgroundColors>();
        public ObservableCollection<Values.BackgroundColors> BackgroundList
        {
            get
            {
                return _backgroundList;
            }
        }

        private Preference _preferenceModel;

        public ReactiveProperty<bool> CanRecording { get; private set; }
        public ReactiveProperty<bool> IsRecordingNow { get; private set; }
        public ReactiveProperty<string> RecButtonLabel { get; private set; }

        public MainWindowViewModel(Preference preference)
        {
            if (preference == null) throw new ArgumentNullException("preference if null.");
            _preferenceModel = preference;

            _backgroundList.Add(Values.BackgroundColors.White);
            _backgroundList.Add(Values.BackgroundColors.Red);
            _backgroundList.Add(Values.BackgroundColors.Green);
            _backgroundList.Add(Values.BackgroundColors.Blue);
            _backgroundList.Add(Values.BackgroundColors.Glay);

            // PreferenceモデルのFeatureAIｓEnabledとバインディング
            FeatureAIsEnabled = _preferenceModel.ToReactivePropertyAsSynchronized(p => p.FeatureAIsEnabled);
            // PreferenceモデルのBackgroundとバインディング
            SelectedBackground = _preferenceModel.ToReactivePropertyAsSynchronized(p => p.Background);


            CanRecording = new ReactiveProperty<bool>(false);
            IsRecordingNow = new ReactiveProperty<bool>(false);
            RecButtonLabel = CanRecording.CombineLatest(IsRecordingNow, (canRecording, isRecordingNow) =>
            {
                // ここではいきなりボタンのラベルの文字列を作ってしまっているが、ボタンのモードみたいなものを返すようにして、Viewで表示を変えるというやり方も考えられる
                if (canRecording)
                {
                    return isRecordingNow ? "録画停止" : "録画開始";
                }
                return "録画不可";
            }).ToReactiveProperty<string>();
        }
    }
}
