﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace PrismAndRxTest
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        private AppContext appContext;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            appContext = new AppContext();
            MainWindow win = new MainWindow(appContext);
            win.Show();

            SubWindow sub = new SubWindow(appContext);
            sub.Show();
        }
    }
}
